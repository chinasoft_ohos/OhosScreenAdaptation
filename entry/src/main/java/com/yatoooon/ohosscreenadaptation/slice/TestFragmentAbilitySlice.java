package com.yatoooon.ohosscreenadaptation.slice;

import com.yatoooon.ohosscreenadaptation.ResourceTable;
import com.yatoooon.ohosscreenadaptation.fragment.TestFragment;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.window.service.WindowManager;

/**
 * TestFragmentAbilitySlice;
 *
 * @since 2021-04-13
 */
public class TestFragmentAbilitySlice extends AbilitySlice {
    private static final int TITLE_COLOR = -13615201;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        super.setUIContent(ResourceTable.Layout_ability_test_fragment);

        ((FractionAbility) getAbility()).getFractionManager()
                .startFractionScheduler()
                .add(ResourceTable.Id_main_container, new TestFragment())
                .submit();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
