package com.yatoooon.ohosscreenadaptation.slice;

import com.yatoooon.ohosscreenadaptation.ResourceTable;
import com.yatoooon.screenadaptation.ScreenAdapterTools;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.service.WindowManager;

/**
 * TestDynamicAddAbilitySlice;
 *
 * @since 2021-04-13
 */
public class TestDynamicAddAbilitySlice extends AbilitySlice {
    private static final int TITLE_COLOR = -13615201;
    private static final int WIDTH_LEFT = 400;
    private static final int WIDTH_RIGHT = 600;
    private static final int COLOR = 204;
    private static final int SIZE = 50;
    private static final int MARGINS = 20;
    private static final int PADDING = 50;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        ComponentContainer layout = createContentLayout(this,ResourceTable.Layout_ability_test_dynamic_add);
        super.setUIContent(layout);

        ScreenAdapterTools.getInstance().loadView(layout);

        Button button = (Button) findComponentById(ResourceTable.Id_bt_addview);
        DependentLayout dependentLayout = (DependentLayout) findComponentById(ResourceTable.Id_rl_addview);

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                component.setVisibility(Component.INVISIBLE);

                Text textView = creatTextView(WIDTH_LEFT, DependentLayout.LayoutConfig.ALIGN_PARENT_LEFT);
                Text textView1 = creatTextView(WIDTH_RIGHT, DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT);

                dependentLayout.addComponent(textView);
                dependentLayout.addComponent(textView1);
            }
        });
    }

    private Text creatTextView(int width, int alignParent) {
        Text textView = new Text(this);

        textView.setTextAlignment(TextAlignment.TOP);

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(COLOR, COLOR, COLOR));
        textView.setBackground(shapeElement);

        textView.setText("Hello World!");
        textView.setTextSize(SIZE, Text.TextSizeType.PX);
        DependentLayout.LayoutConfig lp = new DependentLayout
                .LayoutConfig(width, DependentLayout.LayoutConfig.MATCH_PARENT);
        lp.addRule(alignParent);
        lp.setMargins(MARGINS, MARGINS, MARGINS, MARGINS);
        textView.setPadding(PADDING, PADDING, PADDING, PADDING);
        textView.setLayoutConfig(lp);

        ScreenAdapterTools.getInstance().loadView(textView);
        return textView;
    }

    /**
     * getWindow().getDecorView()
     * 获取最上层component
     *
     * @param abilitySlice
     * @param layoutId
     * @return ComponentContainer
     */

    private ComponentContainer createContentLayout(AbilitySlice abilitySlice, int layoutId) {
        return (ComponentContainer) LayoutScatter.getInstance(abilitySlice).parse(layoutId, null, false);
    }

    private Component getDecorView(Component component) {
        Component rootView = component;

        while (true) {
            Component tempView = (Component)rootView.getComponentParent();
            if (tempView == null) {
                break;
            }
            else {
                rootView = tempView;
            }
        }
        return rootView;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
