package com.yatoooon.ohosscreenadaptation.slice;

import com.yatoooon.ohosscreenadaptation.ResourceTable;
import com.yatoooon.screenadaptation.ScreenAdapterTools;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.window.service.WindowManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * TestAbilitySlice;
 *
 * @since 2021-04-13
 */
public class TestAbilitySlice extends AbilitySlice {
    private static final int TITLE_COLOR = -13615201;

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "TestAbilitySlice");

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        ComponentContainer layout = createContentLayout(this,ResourceTable.Layout_test_px);
        super.setUIContent(layout);

        ScreenAdapterTools.getInstance().reset(getContext());

        ScreenAdapterTools.getInstance().loadView(getDecorView(layout));
    }

    /**
     * getWindow().getDecorView()
     * 获取最上层component
     *
     * @param abilitySlice
     * @param layoutId
     * @return ComponentContainer
     */
    private ComponentContainer createContentLayout(AbilitySlice abilitySlice, int layoutId) {
        return (ComponentContainer) LayoutScatter.getInstance(abilitySlice).parse(layoutId, null, false);
    }

    private Component getDecorView(Component component) {
        Component rootView = component;

        while (true) {
            Component tempView = (Component)rootView.getComponentParent();
            if (tempView == null) {
                break;
            }
            else {
                rootView = tempView;
            }
        }

        return rootView;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
