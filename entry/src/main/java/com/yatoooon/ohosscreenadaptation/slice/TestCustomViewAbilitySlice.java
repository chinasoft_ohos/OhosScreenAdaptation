package com.yatoooon.ohosscreenadaptation.slice;

import com.yatoooon.ohosscreenadaptation.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.window.service.WindowManager;

/**
 * TestCustomViewAbilitySlice;
 *
 * @since 2021-04-13
 */
public class TestCustomViewAbilitySlice extends AbilitySlice {
    private static final int TITLE_COLOR = -13615201;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        super.setUIContent(ResourceTable.Layout_ability_test_custom_view);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
