package com.yatoooon.ohosscreenadaptation.slice;

import com.yatoooon.ohosscreenadaptation.ResourceTable;
import com.yatoooon.screenadaptation.ScreenAdapterTools;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.window.service.WindowManager;

/**
 * MainAbilitySlice;
 *
 * @since 2021-04-13
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private static final int TITLE_COLOR = -13615201;
    private static final float RIPPLE_ALPHA = 0.2f;
    private static final int RIPPLE_DURATION = 2000;
    private static final int RIPPLE_DIAMETERDP = 20;
    private Intent mIntent;
    private Button btActivity;
    private Button btFragment;
    private Button btRecyclerview;
    private Button btCustomview;
    private Button btDynamicAddView;
    private DirectionalLayout dl;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        super.setUIContent(ResourceTable.Layout_ability_main);

        dl = (DirectionalLayout) findComponentById(ResourceTable.Id_dl);
        btActivity = (Button) findComponentById(ResourceTable.Id_bt_activity);
        btFragment = (Button) findComponentById(ResourceTable.Id_bt_fragment);
        btRecyclerview = (Button) findComponentById(ResourceTable.Id_bt_recyclerview);
        btCustomview = (Button) findComponentById(ResourceTable.Id_bt_customview);
        btDynamicAddView = (Button) findComponentById(ResourceTable.Id_bt_dynamicaddview);

        ScreenAdapterTools.getInstance().loadView(dl);

        btActivity.setClickedListener(this);
        btFragment.setClickedListener(this);
        btRecyclerview.setClickedListener(this);
        btCustomview.setClickedListener(this);
        btDynamicAddView.setClickedListener(this);
    }

    /**
     * startIntent
     *
     * @param withAbilityName
     */
    private void startIntent(String withAbilityName) {
        mIntent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.yatoooon.ohosscreenadaptation")
                .withAbilityName(withAbilityName)
                .build();
        mIntent.setOperation(operation);
        startAbility(mIntent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_bt_activity:
                startIntent("com.yatoooon.ohosscreenadaptation.TestAbility");
                break;
            case ResourceTable.Id_bt_fragment:
                startIntent("com.yatoooon.ohosscreenadaptation.TestFragmentAbility");
                break;
            case ResourceTable.Id_bt_recyclerview:
                startIntent("com.yatoooon.ohosscreenadaptation.TestRecyclerViewAbility");
                break;
            case ResourceTable.Id_bt_customview:
                startIntent("com.yatoooon.ohosscreenadaptation.TestCustomViewAbility");
                break;
            case ResourceTable.Id_bt_dynamicaddview:
                startIntent("com.yatoooon.ohosscreenadaptation.TestDynamicAddAbility");
                break;
            default:
                break;
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
