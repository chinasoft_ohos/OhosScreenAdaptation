package com.yatoooon.ohosscreenadaptation.slice;

import com.yatoooon.ohosscreenadaptation.ResourceTable;
import com.yatoooon.ohosscreenadaptation.adapter.RecyclerviewAdapter;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.window.service.WindowManager;

/**
 * TestRecyclerViewAbilitySlice;
 *
 * @since 2021-04-13
 */
public class TestRecyclerViewAbilitySlice extends AbilitySlice {
    private static final int TITLE_COLOR = -13615201;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        super.setUIContent(ResourceTable.Layout_ability_test_recycler_view);

        ListContainer recyclerView = (ListContainer) findComponentById(ResourceTable.Id_recyclerview_test);

        RecyclerviewAdapter recyclerviewAdapter = new RecyclerviewAdapter(this);

        recyclerView.setItemProvider(recyclerviewAdapter);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
