package com.yatoooon.ohosscreenadaptation.adapter;

import com.yatoooon.ohosscreenadaptation.ResourceTable;
import com.yatoooon.screenadaptation.ScreenAdapterTools;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

import java.util.Collections;

/**
 * RecyclerviewAdapter;
 *
 * @since 2021-04-13
 */
public class RecyclerviewAdapter extends BaseItemProvider {
    private static final int COUNT = 5;
    private AbilitySlice slice;

    /**
     * RecyclerviewAdapter
     *
     * @param slice
     */
    public RecyclerviewAdapter(AbilitySlice slice) {
        this.slice = slice;
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public Object getItem(int i) {
        return Collections.emptyList();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        component = LayoutScatter.getInstance(slice)
                .parse(ResourceTable.Layout_test_px, componentContainer, false);
        ScreenAdapterTools.getInstance().loadView(component);
        return component;
    }
}
