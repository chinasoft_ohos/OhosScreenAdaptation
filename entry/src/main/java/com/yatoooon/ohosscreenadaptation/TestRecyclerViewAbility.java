package com.yatoooon.ohosscreenadaptation;

import com.yatoooon.ohosscreenadaptation.slice.TestRecyclerViewAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * TestRecyclerViewAbility;
 *
 * @since 2021-04-13
 */
public class TestRecyclerViewAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TestRecyclerViewAbilitySlice.class.getName());
    }
}
