package com.yatoooon.ohosscreenadaptation.view;

import com.yatoooon.ohosscreenadaptation.ResourceTable;
import com.yatoooon.screenadaptation.ScreenAdapterTools;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

/**
 * TestFragment;
 *
 * @since 2021-04-13
 */
public class CustomView extends DependentLayout {
    /**
     * CustomView
     *
     * @param context
     */
    public CustomView(Context context) {
        this(context, null);
    }

    /**
     * CustomView
     *
     * @param context
     * @param attrs
     */
    public CustomView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * CustomView
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    public CustomView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, String.valueOf(defStyleAttr));
        int layoutCustomview = ResourceTable.Layout_customview;

        Component parse = LayoutScatter.getInstance(getContext()).parse(layoutCustomview, null, false);
        addComponent(parse);

        ScreenAdapterTools.getInstance().loadView(parse);
    }
}

