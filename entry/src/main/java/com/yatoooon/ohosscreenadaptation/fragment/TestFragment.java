package com.yatoooon.ohosscreenadaptation.fragment;

import com.yatoooon.ohosscreenadaptation.ResourceTable;
import com.yatoooon.screenadaptation.ScreenAdapterTools;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * TestFragment;
 *
 * @since 2021-04-13
 */
public class TestFragment extends Fraction {
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component parse = scatter.parse(ResourceTable.Layout_test_px, container, false);
        ScreenAdapterTools.getInstance().loadView(parse);
        return parse;
    }
}
