package com.yatoooon.ohosscreenadaptation;

import com.yatoooon.screenadaptation.ScreenAdapterTools;
import com.yatoooon.screenadaptation.bean.MetaDataBean;

import ohos.aafwk.ability.AbilityPackage;
import ohos.global.configuration.Configuration;

/**
 * MyApplication;
 *
 * @since 2021-04-13
 */
public class MyApplication extends AbilityPackage {
    private static final int WIDTH = 1080;
    private static final int DPI = 480;

    /**
     * onInitialize
     *
     */
    @Override
    public void onInitialize() {
        super.onInitialize();
        MetaDataBean metaDataBean = new MetaDataBean();
        metaDataBean.setDesignwidth(WIDTH);
        metaDataBean.setDesigndpi(DPI);
        metaDataBean.setFontsize(1.0f);
        metaDataBean.setUnit("px");
        ScreenAdapterTools.init(this, metaDataBean);
    }

    @Override
    public void onConfigurationUpdated(Configuration configuration) {
        super.onConfigurationUpdated(configuration);
        ScreenAdapterTools.getInstance().reset(this);
    }
}
