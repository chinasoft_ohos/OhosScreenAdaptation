package com.yatoooon.ohosscreenadaptation;

import com.yatoooon.ohosscreenadaptation.slice.TestFragmentAbilitySlice;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

/**
 * TestFragmentAbility;
 *
 * @since 2021-04-13
 */
public class TestFragmentAbility extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TestFragmentAbilitySlice.class.getName());
        onLeaveForeground();
    }
}
