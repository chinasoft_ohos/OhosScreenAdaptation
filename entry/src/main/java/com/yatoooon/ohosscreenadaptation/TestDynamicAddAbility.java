package com.yatoooon.ohosscreenadaptation;

import com.yatoooon.ohosscreenadaptation.slice.TestDynamicAddAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * TestDynamicAddAbility;
 *
 * @since 2021-04-13
 */
public class TestDynamicAddAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TestDynamicAddAbilitySlice.class.getName());
    }
}
