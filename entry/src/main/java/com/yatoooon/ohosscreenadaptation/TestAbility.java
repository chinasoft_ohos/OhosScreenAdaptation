package com.yatoooon.ohosscreenadaptation;

import com.yatoooon.ohosscreenadaptation.slice.TestAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * TestAbility;
 *
 * @since 2021-04-13
 */
public class TestAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TestAbilitySlice.class.getName());
    }
}
