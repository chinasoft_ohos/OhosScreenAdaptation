package com.yatoooon.ohosscreenadaptation;

import com.yatoooon.ohosscreenadaptation.slice.TestCustomViewAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * TestCustomViewAbility;
 *
 * @since 2021-04-13
 */
public class TestCustomViewAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TestCustomViewAbilitySlice.class.getName());
    }
}
