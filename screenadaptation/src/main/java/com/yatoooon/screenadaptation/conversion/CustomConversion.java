package com.yatoooon.screenadaptation.conversion;

import com.yatoooon.screenadaptation.loadviewhelper.AbsLoadViewHelper;

import ohos.agp.components.Component;

/**
 * CustomConversion;
 *
 * @since 2021-04-13
 */
public class CustomConversion implements IConversion {
    @Override
    public void transform(Component view, AbsLoadViewHelper loadViewHelper) {
        if (view.getLayoutConfig() != null) {
            loadViewHelper.loadWidthHeightFont(view);
            loadViewHelper.loadPadding(view);
            loadViewHelper.loadLayoutMargin(view);
            loadViewHelper.loadMaxWidthAndHeight(view);
            loadViewHelper.loadMinWidthAndHeight(view);
        }
    }
}
