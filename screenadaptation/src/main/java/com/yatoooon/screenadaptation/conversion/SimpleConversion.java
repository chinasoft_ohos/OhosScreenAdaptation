package com.yatoooon.screenadaptation.conversion;

import com.yatoooon.screenadaptation.loadviewhelper.AbsLoadViewHelper;

import ohos.agp.components.Component;

/**
 * Only adapter width/height/padding/margin
 * Created by zhangyuwan0 on 2018/3/21.
 *
 * @since 2021-04-13
 */
public class SimpleConversion implements IConversion {
    @Override
    public void transform(Component view, AbsLoadViewHelper loadViewHelper) {
        if (view.getLayoutConfig() != null) {
            loadViewHelper.loadWidthHeightFont(view);
            loadViewHelper.loadPadding(view);
            loadViewHelper.loadLayoutMargin(view);
        }
    }
}
