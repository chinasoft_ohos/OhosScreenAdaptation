package com.yatoooon.screenadaptation.conversion;

import com.yatoooon.screenadaptation.loadviewhelper.AbsLoadViewHelper;

import ohos.agp.components.Component;

/**
 * IConversion;
 *
 * @since 2021-04-13
 */
public interface IConversion {
    /**
     * transform
     *
     * @param view
     * @param loadViewHelper
     */
    void transform(Component view, AbsLoadViewHelper loadViewHelper);
}
