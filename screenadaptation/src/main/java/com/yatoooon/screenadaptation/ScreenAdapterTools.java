package com.yatoooon.screenadaptation;

import com.yatoooon.screenadaptation.bean.MetaDataBean;
import com.yatoooon.screenadaptation.loadviewhelper.AbsLoadViewHelper;
import com.yatoooon.screenadaptation.loadviewhelper.LoadViewHelper;

import ohos.app.Context;

/**
 * ScreenAdapterTools;
 *
 * @since 2021-04-13
 */
public class ScreenAdapterTools {
    private static AbsLoadViewHelper sLoadViewHelper;

    public static AbsLoadViewHelper getInstance() {
        return sLoadViewHelper;
    }

    /**
     * init
     *
     * @param context
     * @param metaDataBean
     */
    public static void init(Context context, MetaDataBean metaDataBean) {
        init(context, new Iprovider() {
            @Override
            public AbsLoadViewHelper provide(Context context, int designWidth,
                                             int designDpi, float fontSize, String unit) {
                return new LoadViewHelper(context, designWidth, designDpi, fontSize, unit);
            }
        },metaDataBean);
    }

    /**
     * init
     *
     * @param context
     * @param provider
     * @param metaDataBean
     */
    public static void init(Context context, Iprovider provider, MetaDataBean metaDataBean) {
        int designwidth = metaDataBean.getDesignwidth();
        int designdpi = metaDataBean.getDesigndpi();
        float fontsize = metaDataBean.getFontsize();
        String unit = metaDataBean.getUnit();
        sLoadViewHelper = provider.provide(context, designwidth, designdpi, fontsize, unit);
    }

    /**
     * Iprovider;
     *
     * @since 2021-04-13
     */
    public interface Iprovider {
        /**
         * provide
         *
         * @param context
         * @param designWidth
         * @param designDpi
         * @param fontSize
         * @param unit
         * @return AbsLoadViewHelper
         */
        AbsLoadViewHelper provide(Context context, int designWidth, int designDpi, float fontSize, String unit);
    }
}
