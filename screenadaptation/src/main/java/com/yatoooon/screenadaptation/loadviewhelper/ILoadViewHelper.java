package com.yatoooon.screenadaptation.loadviewhelper;

import ohos.agp.components.Component;

/**
 * ILoadViewHelper;
 *
 * @since 2021-04-13
 */
public interface ILoadViewHelper {
    /**
     * loadWidthHeightFont
     *
     * @param view
     */
    void loadWidthHeightFont(Component view);

    /**
     * loadPadding
     *
     * @param view
     */
    void loadPadding(Component view);

    /**
     * loadLayoutMargin
     *
     * @param view
     */
    void loadLayoutMargin(Component view);

    /**
     * loadMaxWidthAndHeight
     *
     * @param view
     */
    void loadMaxWidthAndHeight(Component view);

    /**
     * loadMinWidthAndHeight
     *
     * @param view
     */
    void loadMinWidthAndHeight(Component view);

    /**
     * loadCustomAttrValue
     *
     * @param px
     * @return int
     */
    int loadCustomAttrValue(int px);
}
