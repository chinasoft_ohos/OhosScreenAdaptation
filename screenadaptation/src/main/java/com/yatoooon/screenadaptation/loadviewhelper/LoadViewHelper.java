package com.yatoooon.screenadaptation.loadviewhelper;

import com.yatoooon.screenadaptation.utils.ViewUtils;
import com.yatoooon.screenadaptation.utils.dp2pxUtils;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * LoadViewHelper;
 *
 * @since 2021-04-13
 */
public class LoadViewHelper extends AbsLoadViewHelper {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "LoadViewHelper");
    private static final int DPI = 160;

    /**
     * LoadViewHelper
     *
     * @param context
     * @param designWidth
     * @param designDpi
     * @param fontSize
     * @param unit
     */
    public LoadViewHelper(Context context, int designWidth, int designDpi, float fontSize, String unit) {
        super(context, designWidth, designDpi, fontSize, unit);
    }

    @Override
    public void loadWidthHeightFont(Component view) {
        ComponentContainer.LayoutConfig layoutParams = view.getLayoutConfig();
        if (layoutParams.width > 0) {
            layoutParams.width = setValue(layoutParams.width);
        }
        if (layoutParams.height > 0) {
            layoutParams.height = setValue(layoutParams.height);
        }
        loadViewFont(view);
    }

    private void loadViewFont(Component view) {
        if (view instanceof Text) {
            Text textView = (Text) view;
            ((Text) view).setTextSize((int) setFontSize(textView), Text.TextSizeType.PX);
        }
    }

    private float setFontSize(Text textView) {
        return calculateValue(textView.getTextSize() * fontSize);
    }

    @Override
    public void loadPadding(Component view) {
        view.setPadding(setValue(view.getPaddingLeft()), setValue(view.getPaddingTop()),
                setValue(view.getPaddingRight()), setValue(view.getPaddingBottom()));
    }

    @Override
    public void loadLayoutMargin(Component view) {
        ComponentContainer.LayoutConfig params = view.getLayoutConfig();
        if (params.getMargins() != null) {
            params.setMarginLeft(setValue(params.getMarginLeft()));
            params.setMarginRight(setValue(params.getMarginRight()));
            params.setMarginTop(setValue(params.getMarginTop()));
            params.setMarginBottom(setValue(params.getMarginBottom()));
            view.setLayoutConfig(params);
        }
    }

    @Override
    public void loadMaxWidthAndHeight(Component view) {
        ViewUtils.setMaxWidth(view, setValue(ViewUtils.getMaxWidth(view)));
        ViewUtils.setMaxHeight(view, setValue(ViewUtils.getMaxHeight(view)));
    }

    @Override
    public void loadMinWidthAndHeight(Component view) {
        ViewUtils.setMinWidth(view, setValue(ViewUtils.getMinWidth(view)));
        ViewUtils.setMinHeight(view, setValue(ViewUtils.getMinHeight(view)));
    }

    @Override
    public int loadCustomAttrValue(int px) {
        return setValue(px);
    }

    private int setValue(int value) {
        HiLog.info(LABEL, "value：" + value);

        if (value == 0) {
            return 0;
        } else if (value == 1) {
            return 1;
        }
        return (int) calculateValue(value);
    }

    private float calculateValue(float value) {
        HiLog.info(LABEL, "unit：" + unit);
        HiLog.info(LABEL, "calculateValue Value：" + value);
        if ("px".equals(unit)) {
            HiLog.info(LABEL, "actualWidth: " + actualWidth + " designWidth：" + designWidth);

            HiLog.info(LABEL, "return:" + value * (actualWidth / (float) designWidth));

            return value * (actualWidth / (float) designWidth);
        } else if ("vp".equals(unit)) {
            int dip = dp2pxUtils.px2vp(actualDensity, value);
            value = ((float) designDpi / DPI) * dip;
            return value * (actualWidth / (float) designWidth);
        }
        return 0;
    }
}
