/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yatoooon.screenadaptation.bean;

/**
 * MetaDataBean;
 *
 * @since 2021-04-13
 */
public class MetaDataBean {
    private static final int WIDTH = 1080;
    private static final int DPI = 480;
    private int designwidth = WIDTH;
    private int designdpi = DPI;
    private float fontsize = 1.0f;
    private String unit = "px";

    public int getDesignwidth() {
        return designwidth;
    }

    public void setDesignwidth(int designwidth) {
        this.designwidth = designwidth;
    }

    public int getDesigndpi() {
        return designdpi;
    }

    public void setDesigndpi(int designdpi) {
        this.designdpi = designdpi;
    }

    public float getFontsize() {
        return fontsize;
    }

    public void setFontsize(float fontsize) {
        this.fontsize = fontsize;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
