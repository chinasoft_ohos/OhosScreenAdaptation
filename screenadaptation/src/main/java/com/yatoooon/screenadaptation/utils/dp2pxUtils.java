package com.yatoooon.screenadaptation.utils;

import static ohos.agp.components.AttrHelper.getDensity;

import ohos.agp.components.AttrHelper;
import ohos.app.Context;

/**
 * dp2pxUtils;
 *
 * @since 2021-04-13
 */
public class dp2pxUtils {
    private static final String TAG = "vp2pxUtils";
    private static final float FLOAT = 0.5f;

    /**
     * vp2px
     *
     * @param context
     * @param dpValue
     * @return int
     */
    public static int vp2px(Context context, float dpValue) {
        return AttrHelper.vp2px(dpValue, getDensity(context));
    }

    /**
     * vp2px
     *
     * @param density
     * @param dpValue
     * @return int
     */
    public static int vp2px(float density, float dpValue) {
        return AttrHelper.vp2px(dpValue, density);
    }

    /**
     * px2vp
     *
     * @param context
     * @param pxValue
     * @return int
     */
    public static int px2vp(Context context, float pxValue) {
        final float scale = getDensity(context);
        return (int) (pxValue / scale + FLOAT);
    }

    /**
     * px2vp
     *
     * @param density
     * @param pxValue
     * @return int
     */
    public static int px2vp(float density, float pxValue) {
        return (int) (pxValue / density + FLOAT);
    }
}
