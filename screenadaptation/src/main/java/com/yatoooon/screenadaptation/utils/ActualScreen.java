package com.yatoooon.screenadaptation.utils;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

/**
 * ActualScreen;
 *
 * @since 2021-04-13
 */
public class ActualScreen {
    /**
     * screenInfo
     *
     * @param context
     * @return float[]
     */
    public static float[] screenInfo(Context context) {
        Optional<Display> optional = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = optional.get().getAttributes();
        return new float[]{displayAttributes.width, displayAttributes.height,
            displayAttributes.densityPixels, displayAttributes.densityDpi};
    }
}

