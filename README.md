# OhosScreenAdaptation

#### 项目介绍
- 项目名称：OhosScreenAdaptation

- 所属系列：openharmony的第三方组件适配移植

- 功能：屏幕分辨率适配

- 项目移植状态：完成

- 调用差异：无

- 开发版本：sdk6，DevEco Studio 2.2 Beta1

- 基线版本：master分支

#### 效果演示
![输入图片说明](https://gitee.com/chinasoft_ohos/OhosScreenAdaptation/raw/master/gif/demo.gif  "demo.gif")

#### 安装教程
1.在项目根目录下的build.gradle文件中，
```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```
2.在entry模块的build.gradle文件中，
```gradle
dependencies {
   implementation('com.gitee.chinasoft_ohos:OhosScreenAdaptation:1.0.0')
   ......  
}
```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

 1. 初始化屏幕:
  ```java
  //在继承AbilityPackage类中，重写onInitialize()方法内初始化
    MetaDataBean metaDataBean = new MetaDataBean();
        metaDataBean.setDesignwidth(1080);
        metaDataBean.setDesigndpi(480);
        metaDataBean.setFontsize(1.0f);
        metaDataBean.setUnit("px");
        ScreenAdapterTools.init(this, metaDataBean);
  ```

 2. 在AbilitySlice中调用方式:
  ```java
    ScreenAdapterTools.getInstance().loadView(getDecorView(layout));
  ```

 3. 在Fraction，自定义view，ListContainer等等中调用方式:
    ```java
    public class TestFragment extends Fraction {public class TestFragment extends Fraction {
        @Override
        protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
            Component parse = scatter.parse(ResourceTable.Layout_test_px, container, false);
            ScreenAdapterTools.getInstance().loadView(parse);
            return parse;
        }
    }
    //自定义View
    public class CustomView extends DependentLayout {
        public CustomView(Context context, AttrSet attrs, int defStyleAttr) {
            super(context, attrs, String.valueOf(defStyleAttr));
            int layoutCustomview = ResourceTable.Layout_customview;
            Component parse = LayoutScatter.getInstance(getContext()).parse(layoutCustomview, null, false);
            addComponent(parse);
            ScreenAdapterTools.getInstance().loadView(parse);
        }
    }
    //ListContainer
    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        component = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_test_px, componentContainer, false);
        ScreenAdapterTools.getInstance().loadView(component);
        return component;
    }
    ```
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息
```

   Copyright 2018 yatoooon

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```